import { useDispatch } from "react-redux";
import { changeStatus } from "../redux/reducers/todosReducer";

const ListItem = ({ todo }) => {
  const dispatch = useDispatch();

  const changeStatusTodo = (todo) => {
    dispatch(changeStatus(todo));
  };

  return (
    <li
      key={todo.id}
      onClick={() => changeStatusTodo(todo)}
      style={{ textDecoration: todo.completed ? "line-through" : "none" }}
    >
      {todo.title}
    </li>
  );
};

export default ListItem;
