import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, selectTodos } from "./redux/reducers/todosReducer";
import ListItem from "./Components/ListItem";
import { setFilter } from "./redux/reducers/filterReducer";
import { fetchTodos } from "./redux/actions/fetchTodos";

function App() {
  const status = useSelector((x) => x);
  console.log(status);

  const entity = useSelector(selectTodos);
  console.log(entity);

  const dispatch = useDispatch();

  const [inputValue, setInputValue] = useState("");

  const handleChangeInput = (e) => {
    setInputValue(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const todo = {
      id: Math.random().toString(36),
      title: inputValue,
      completed: false,
    };
    dispatch(addTodo(todo));
    setInputValue("");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input value={inputValue} onChange={(e) => handleChangeInput(e)} />
      </form>
      <div>
        <button onClick={() => dispatch(setFilter("all"))}>Show TODOS</button>
        <button onClick={() => dispatch(setFilter("complete"))}>
          Show Completed
        </button>
        <button onClick={() => dispatch(setFilter("incomplete"))}>
          Show Incompleted
        </button>
        <button onClick={() => dispatch(fetchTodos())}>Fetch</button>
      </div>
      <ul>
        {entity &&
          entity.map((todo, index) => <ListItem todo={todo} key={index} />)}
      </ul>
    </div>
  );
}

export default App;
