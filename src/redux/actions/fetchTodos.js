import {
  FETCH_FULFILLED,
  FETCH_PENDING,
  FETCH_REJECTED,
} from "../reducers/fetchingReducer";

export const fetchTodos = () => async (dispatch) => {
  dispatch({ type: FETCH_PENDING });
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/todos");
    const data = await response.json();
    const todos = data.slice(0, 10);
    dispatch({ type: FETCH_FULFILLED, payload: todos });
  } catch (error) {
    dispatch({ type: FETCH_REJECTED, error: error.message });
  }
};
