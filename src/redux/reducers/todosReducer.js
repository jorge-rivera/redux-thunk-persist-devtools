const initialState = [];

// Actions Creators
export const addTodo = (payload) => ({
  type: "todos/add",
  payload,
});

export const changeStatus = (payload) => ({
  type: "todos/changeStatus",
  payload,
});

const todosReducer = (state = initialState, action) => {
  switch (action.type) {
    case "todos/fulfilled": {
      return action.payload;
    }

    case "todos/add": {
      return [...state.concat(action.payload)];
    }

    case "todos/changeStatus": {
      const newTodos = state.map((todo) => {
        if (todo.id === action.payload.id) {
          return {
            ...todo,
            completed: !todo.completed,
          };
        }
        return todo;
      });
      return newTodos;
    }

    default:
      return state;
  }
};

export const selectTodos = (state) => {
  const {
    entity: { todos },
  } = state;

  if (state.filter === "complete") {
    return todos.filter((todo) => todo.completed);
  }
  if (state.filter === "incomplete") {
    return todos.filter((todo) => !todo.completed);
  }
  return todos;
};

export default todosReducer;
