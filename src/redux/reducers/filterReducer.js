const filterInitialState = "all";

// Action Creators
export const setFilter = (setStatus) => ({
  type: "filter/set",
  payload: setStatus,
});

const filterReducer = (state = filterInitialState, action) => {
  switch (action.type) {
    case "filter/set": {
      return action.payload;
    }

    default:
      return state;
  }
};

export default filterReducer;
