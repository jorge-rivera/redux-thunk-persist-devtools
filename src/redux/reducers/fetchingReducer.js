// idle, pending, fulfilled, error => rejected
const fetchingInitialState = {
  loading: "idle",
  error: null,
};

export const FETCH_PENDING = "todos/pending";
export const FETCH_FULFILLED = "todos/fulfilled";
export const FETCH_REJECTED = "todos/rejected";

const fetchingReducer = (state = fetchingInitialState, action) => {
  switch (action.type) {
    case FETCH_PENDING: {
      return {
        ...state,
        loading: "pending",
      };
    }

    case FETCH_FULFILLED: {
      return {
        ...state,
        loading: "fulfilled",
      };
    }

    case FETCH_REJECTED: {
      return {
        loading: "rejected",
        error: action.error,
      };
    }

    default:
      return state;
  }
};

export default fetchingReducer;
