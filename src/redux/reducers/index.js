import { combineReducers } from "redux";
import todosReducer from "./todosReducer";
import filterReducer from "./filterReducer";
import fetchingReducer from "./fetchingReducer";

const rootReducer = combineReducers({
  entity: combineReducers({
    todos: todosReducer,
    status: fetchingReducer,
  }),
  filter: filterReducer,
});

export default rootReducer;
